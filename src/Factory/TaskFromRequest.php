<?php

namespace App\Factory;

use Domain\ToDo\Model\Task;
use Domain\ToDo\Model\TaskValidator;
use Domain\Shared\Exception\ValidationException;

class TaskFromRequest {

    static function create($input) {
        $task = new Task();

        $task->setId($input->get('id'));
        $task->setTitle($input->get('title'));
        $task->setDescription($input->get('description'));

        $validator = TaskValidator::getInstance();

        if (!$validator->validate($task)) {
            throw new ValidationException('Invalid input', 0, null, $validator->getErrors());
        }

        return $task;
    }

}
