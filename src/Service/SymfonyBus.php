<?php

namespace App\Service;

use Domain\Shared\Job\Bus;

class SymfonyBus extends Bus {

    private $service_container;

    public function __construct($container) {
        $this->service_container = $container;
    }

    public function getService($name) {
        return $this->service_container->get($name);
    }

}
