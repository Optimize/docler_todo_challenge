<?php

namespace App\Serializer;

use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Domain\Shared\Exception\DomainException;

class ExceptionNormalizer implements NormalizerInterface {

    public function normalize($exception, string $format = null, array $context = []) {
        $original_exception = $context['exception'];

        $hints = [];
        if ($original_exception instanceof DomainException) {
            $hints = $original_exception->getHints();
        }
        return [
            'result' => 'Error',
            'data' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getStatusCode(),
            ],
            'hints' => $hints,
        ];
    }

    public function supportsNormalization($data, string $format = null) {
        return $data instanceof FlattenException;
    }

}
