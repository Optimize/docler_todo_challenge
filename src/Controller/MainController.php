<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MainController extends APIController {

    public function index(): JsonResponse {
        return $this->sendOk('OK', 'Docler ToDo Challenge');
    }

    public function doc(): Response {
        $data = file_get_contents(__DIR__ . '/../Resource/doc.yml');
        return new Response($data);
    }

}
