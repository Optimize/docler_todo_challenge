<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class APIController extends AbstractController {

    public function send($result, $data, $status): JsonResponse {
        return new JsonResponse([
            'result' => $result,
            'data' => $data,
            'hints' => [],
                ], $status);
    }

    public function sendOk($result, $data): JsonResponse {
        return $this->send($result, $data, 200);
    }

    public function sendNotFound($data): JsonResponse {
        return $this->send($result, $data, 404);
    }

}
