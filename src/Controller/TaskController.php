<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//
use Domain\Shared\Job\Bus;
//
use Domain\ToDo\Job\GetTasks;
use Domain\ToDo\Job\GetTaskDetails;
use Domain\ToDo\Job\CreateTask;
use Domain\ToDo\Job\UpdateTask;
use Domain\ToDo\Job\DeleteTask;
use Domain\ToDo\Job\MarkTaskAsDone;
//
use \App\Factory\TaskFromRequest;

class TaskController extends APIController {

    protected $bus;

    public function __construct(Bus $bus) {

        $this->bus = $bus;
    }

    public function all(): JsonResponse {

        $job = new GetTasks();
        $tasks = $this->bus->dispatch($job);

        return $this->sendOk('OK', $tasks);
    }

    public function details($id): JsonResponse {

        $job = new GetTaskDetails($id);
        $task = $this->bus->dispatch($job);

        return $this->sendOk('OK', $task);
    }

    public function create(Request $request): JsonResponse {

        $task = TaskFromRequest::create($request);

        $job = new CreateTask($task);
        $id = $this->bus->dispatch($job);

        return $this->send('Created', ['id' => $id], 201);
    }

    public function update(Request $request): JsonResponse {

        $task = TaskFromRequest::create($request);

        $job = new UpdateTask($task);
        $id = $this->bus->dispatch($job);

        return $this->sendOk('Updated', ['id' => $id]);
    }

    public function markAsDone($id): JsonResponse {

        $job = new MarkTaskAsDone($id);
        $id_result = $this->bus->dispatch($job);

        return $this->sendOk('Done', ['id' => $id_result]);
    }

    public function delete($id): JsonResponse {

        $job = new DeleteTask($id);

        $id_result = $this->bus->dispatch($job);

        return $this->sendOk('Deleted', ['id' => $id_result]);
    }

}
