<?php

namespace App\Repository;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait InFileRepository {

    public function getIds() {
        $directory = $this->getBaseDir();
        $filenames = glob($directory . "*.dat");

        $ids = array_map(function($e) {
            return str_replace('.dat', '', basename($e));
        }, $filenames);

        return $ids;
    }

    public function write($id, $data) {
        $filename = $this->getFilename($id);

        file_put_contents($filename, serialize($data));
    }

    public function read($id) {
        $filename = $this->getFilename($id);
        $this->ensureExists($filename);

        $data = unserialize(file_get_contents($filename));

        return $data;
    }

    public function remove($id) {
        $filename = $this->getFilename($id);

        $this->ensureExists($filename);

        unlink($filename);

        return $id;
    }

    protected function ensureIdExists($id) {
        $filename = $this->getFilename($id);

        return $this->ensureExists($filename);
    }

    protected function ensureExists($filename) {
        if (!file_exists($filename)) {
            throw new NotFoundHttpException('Task does not exist.');
        }
    }

    protected function getFilename($id) {
        return $this->getBaseDir() . $id . '.dat';
    }

    protected function getBaseDir() {
        return dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR
                . 'data' . DIRECTORY_SEPARATOR .
                $this->getSubdirectory() . DIRECTORY_SEPARATOR;
    }

    abstract function getSubdirectory();
}
