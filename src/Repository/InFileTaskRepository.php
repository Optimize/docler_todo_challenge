<?php

namespace App\Repository;

use Domain\ToDo\Model\Task;

class InFileTaskRepository {

    use InFileRepository;

    public function getSubdirectory() {
        return 'tasks';
    }

    /*
     * class members
     */

    public function getAll() {
        $ids = $this->getIds();
        $tasks = [];

        foreach ($ids as $id) {
            $tasks[] = $this->read($id);
        }

        return $tasks;
    }

    public function create(Task $task) {
        $task->setId(uniqid());
        $this->write($task->getId(), $task);

        return $task->getId();
    }

    public function get($id) {
        return $this->read($id);
    }

    public function update(Task $task) {
        $this->ensureIdExists($task->getId());

        $this->write($task->getId(), $task);

        return $task->getId();
    }

    public function delete($id) {
        $this->remove($id);

        return $id;
    }

    public function setDone($id) {
        $task = $this->get($id);

        $task->setDone(true);

        $this->update($task);

        return $id;
    }

}
