Docler ToDo Challenge
===

Intro
---

This project is designed using a simplified version of the CQS pattern. 
Dependency inversion and autowiring is achieved using services (see config/services.yml).


Usage
---

Browse to http://yourserver/doc to get the service description.
Alternatively, you can use the postman collection provided in the root directory.


Testing 
---

In the root directory, run:

php .\bin\phpunit 


