<?php

namespace Domain\Shared\Exception;

abstract class DomainException extends \Exception {

    protected $hints;

    function __construct(string $message = "", int $code = 0, \Throwable $previous = NULL, array $hints = []) {
        parent::__construct($message, $code, $previous);

        $this->hints = $hints;
    }

    function getHints() {
        return $this->hints;
    }

}
