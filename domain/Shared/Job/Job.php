<?php

namespace Domain\Shared\Job;

interface Job {

  function getName();
}
