<?php

namespace Domain\Shared\Job;

abstract class Handler {

    /**
     * @var Domain\Shared\Job\Bus
     */
    protected $bus;

    function __construct(Bus $bus) {
        $this->bus = $bus;
    }

}
