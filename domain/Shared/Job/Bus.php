<?php

namespace Domain\Shared\Job;

use Domain\Shared\Job\Job;

abstract class Bus {

  public abstract function getService($name);

  public function dispatch(Job $job) {

    $handler_class = get_class($job) . 'Handler';
    $handler = new $handler_class($this);

    return $handler->handle($job);
  }

}
