<?php

namespace Domain\Shared\Model;

use Domain\Shared\Model\Entity;

abstract class EntityValidator {

    protected $errors;

    protected function __construct() {
        
    }

    final protected function __clone() {
        
    }

    final public static function getInstance(): EntityValidator {

        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        }

        return $instance;
    }

    abstract function getDefinition(): array;

    function getErrors(): array {
        return $this->errors;
    }

    function validate(Entity $entity): bool {

        $constraints = $this->getDefinition();

        $this->errors = [];

        foreach ($constraints as $attribute => $rules) {

            $accessor = 'get' . ucfirst($attribute);

            $value = $entity->$accessor();

            foreach ($rules as $rule) {
                if (!$rule->assert($value)) {
                    $this->errors[$attribute][] = $rule->getMessage();
                }
            }
        }

        return count($this->errors) == 0;
    }

}
