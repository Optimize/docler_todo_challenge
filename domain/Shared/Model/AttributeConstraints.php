<?php

namespace Domain\Shared\Model;

use Domain\Shared\Model\Constraints\Required;
use Domain\Shared\Model\Constraints\StringType;
use Domain\Shared\Model\Constraints\BooleanType;
use Domain\Shared\Model\Constraints\MinLength;
use Domain\Shared\Model\Constraints\MaxLength;

class AttributeConstraints {

    public static function string() {
        return new StringType();
    }

    public static function boolean() {
        return new BooleanType();
    }

    public static function required() {
        return new Required();
    }

    public static function minLength($value) {
        return new MinLength($value);
    }

    public static function maxLength($value) {
        return new MaxLength($value);
    }

}
