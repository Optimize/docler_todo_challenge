<?php

namespace Domain\Shared\Model\Constraints;

abstract class Constraint {

    abstract function assert($value);

    abstract function getMessage(): string;
}
