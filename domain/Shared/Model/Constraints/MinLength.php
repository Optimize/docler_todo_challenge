<?php

namespace Domain\Shared\Model\Constraints;

class MinLength extends Constraint {

    protected $value;

    function __construct($value) {
        $this->value = $value;
    }

    public function assert($value) {
        if (null == $value) {
            return true;
        }

        if (is_string($value)) {
            return strlen($value) >= $this->value;
        } else {
            return count($value) >= $this->value;
        }
    }

    public function getMessage(): string {
        return 'must be greater or equal than ' . $this->value;
    }

}
