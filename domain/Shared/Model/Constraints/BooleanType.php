<?php

namespace Domain\Shared\Model\Constraints;

class BooleanType extends Constraint {

    public function assert($value) {
        if (null == $value) {
            return true;
        }

        return is_bool($value);
    }

    public function getMessage(): string {
        return 'must be a boolean';
    }

}
