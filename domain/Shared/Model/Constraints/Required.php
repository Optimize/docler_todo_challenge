<?php

namespace Domain\Shared\Model\Constraints;

class Required extends Constraint {

    public function assert($value) {
        return $value !== null;
    }

    public function getMessage(): string {
        return 'is required';
    }

}
