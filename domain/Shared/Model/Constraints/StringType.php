<?php

namespace Domain\Shared\Model\Constraints;

class StringType extends Constraint {

    public function assert($value) {
        if (null == $value) {
            return true;
        }

        return is_string($value);
    }

    public function getMessage(): string {
        return 'must be a string';
    }

}
