<?php

namespace Domain\ToDo\Repository;

interface TaskRepository {

  function create($task);

  function getAll();

  function get($id);

  function update($task);

  function delete($id);

  function setDone($id);
}
