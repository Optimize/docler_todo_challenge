<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Query;

class GetTasks implements Query {

  public function getName() {
    return 'task_get';
  }

}
