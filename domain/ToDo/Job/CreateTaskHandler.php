<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class CreateTaskHandler extends Handler {

  public function handle(CreateTask $job) {

    $repository = $this->bus->getService('task_repository');

    return $repository->create($job->getTask());
  }

}
