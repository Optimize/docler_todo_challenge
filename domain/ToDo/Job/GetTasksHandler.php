<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class GetTasksHandler extends Handler {

  public function handle(GetTasks $job) {
    $repository = $this->bus->getService('task_repository');

    return $repository->getAll();
  }

}
