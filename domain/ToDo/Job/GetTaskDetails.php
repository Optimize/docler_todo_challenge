<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Query;

class GetTaskDetails implements Query {

  private $id;

  public function __construct($id) {
    $this->id = $id;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return 'task_details';
  }

}
