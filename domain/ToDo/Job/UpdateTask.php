<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Order;
use \Domain\ToDo\Model\Task;

class UpdateTask implements Order {

  private $task;

  function __construct(Task $task) {
    $this->task = $task;
  }

  /**
   * @return Task
   */
  public function getTask() {
    return $this->task;
  }

  public function getName() {
    return 'task_update';
  }

}
