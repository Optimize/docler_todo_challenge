<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Order;

class MarkTaskAsDone implements Order {

  private $id;

  function __construct($id) {
    $this->id = $id;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return 'task_done';
  }

}
