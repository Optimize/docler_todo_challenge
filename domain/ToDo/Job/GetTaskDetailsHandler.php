<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class GetTaskDetailsHandler extends Handler {

  public function handle(GetTaskDetails $job) {
    $repository = $this->bus->getService('task_repository');

    return $repository->get($job->getId());
  }

}
