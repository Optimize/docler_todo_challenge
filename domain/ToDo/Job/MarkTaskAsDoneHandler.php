<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class MarkTaskAsDoneHandler extends Handler {

  public function handle(MarkTaskAsDone $job) {
    $repository = $this->bus->getService('task_repository');

    return $repository->setDone($job->getId());
  }

}
