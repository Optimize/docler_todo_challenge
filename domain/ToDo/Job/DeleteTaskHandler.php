<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class DeleteTaskHandler extends Handler {

  public function handle(DeleteTask $job) {
    $repository = $this->bus->getService('task_repository');

    return $repository->delete($job->getId());
  }

}
