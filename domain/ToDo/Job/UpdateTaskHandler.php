<?php

namespace Domain\ToDo\Job;

use Domain\Shared\Job\Handler;

class UpdateTaskHandler extends Handler {

  public function handle(UpdateTask $job) {
    $repository = $this->bus->getService('task_repository');

    return $repository->update($job->getTask());
  }

}
