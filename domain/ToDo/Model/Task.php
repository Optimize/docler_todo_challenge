<?php

namespace Domain\ToDo\Model;

use Domain\Shared\Model\Entity;

class Task extends Entity implements \JsonSerializable {

    private $id;
    private $title;
    private $description;
    private $done;

    function __construct() {
        $this->done = false;
    }

    function getId() {
        return $this->id;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function getTitle() {
        return $this->title;
    }

    function setTitle($title): void {
        $this->title = $title;
    }

    function getDescription() {
        return $this->description;
    }

    function setDescription($description): void {
        $this->description = $description;
    }

    function getDone() {
        return $this->done;
    }

    function setDone($done): void {
        $this->done = $done;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'done' => $this->getDone(),
        ];
    }

}
