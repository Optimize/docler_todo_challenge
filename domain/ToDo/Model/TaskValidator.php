<?php

namespace Domain\ToDo\Model;

use Domain\Shared\Model\AttributeConstraints as Assert;
use Domain\Shared\Model\EntityValidator;

class TaskValidator extends EntityValidator {

    public function getDefinition(): array {
        return [
            'id' => [
                Assert::string(),
            ],
            'title' => [
                Assert::string(), Assert::required(),
                Assert::minLength(3), Assert::maxLength(100),
            ],
            'description' => [
                Assert::string(), Assert::required(),
                Assert::minLength(10), Assert::maxLength(500),
            ],
            'done' => [
                Assert::boolean(),
            ]
                ]
        ;
    }

}
