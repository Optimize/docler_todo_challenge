<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase {

    public function testGetAll() {
        $client = static::createClient();

        $client->request('GET', '/tasks');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);

        $this->assertIsArray($response_content['data']);
    }

    public function testCreateUpdateDoneGetDelete() {
        $client = static::createClient();

        $new_title = 'New test';
        $new_description = 'New description';
        $updated_title = 'Updated task';
        $updated_description = 'Updated description';

        $id = $this->createTaskAndAssert($client, $new_title, $new_description);

        $this->updateTaskAndAssert($client, $id, $updated_title, $updated_description);

        $this->markTaskAsDoneAndAssert($client, $id);

        $this->getTaskAndAssert($client, $id, $updated_title, $updated_description);

        $this->deleteTaskAndAssert($client, $id);
    }

    private function createTaskAndAssert($client, $new_title, $new_description): string {
        $client->request('POST', '/task', [
            'title' => $new_title,
            'description' => $new_description
        ]);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);

        return $response_content['data']['id'];
    }

    private function updateTaskAndAssert($client, $id, $updated_title, $updated_description): void {
        $client->request('PUT', "/task/$id", [
            'title' => $updated_title,
            'description' => $updated_description
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);

        $id_updated = $response_content['data']['id'];

        $this->assertEquals($id, $id_updated);
    }

    private function markTaskAsDoneAndAssert($client, $id): void {
        $client->request('PUT', "/task/$id/done");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);

        $id_done = $response_content['data']['id'];

        $this->assertEquals($id, $id_done);
    }

    private function getTaskAndAssert($client, $id, $updated_title, $updated_description): void {
        $client->request('GET', "/task/$id");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);

        $task = $response_content['data'];

        $this->assertEquals($task['id'], $id);
        $this->assertEquals($task['title'], $updated_title);
        $this->assertEquals($task['description'], $updated_description);
        $this->assertEquals($task['done'], true);
    }

    private function deleteTaskAndAssert($client, $id): void {
        $client->request('DELETE', "/task/$id");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response_content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $response_content);
    }

}
